import java.sql.SQLException;
import java.util.ArrayList;

import net.golui.pokemonbattle.battle.BattlePokemon;
import net.golui.pokemonbattle.battle.SingleBattle;
import net.golui.pokemonbattle.battle.Turn;
import net.golui.pokemonbattle.db.DatabaseReader;
import net.golui.pokemonbattle.pokemon.EnumNature;
import net.golui.pokemonbattle.pokemon.Move;
import net.golui.pokemonbattle.pokemon.Pokemon;
import net.golui.pokemonbattle.pokemon.Type;
import net.golui.pokemonbattle.trigger.Trigger;
import net.golui.pokemonbattle.trigger.TriggerFunctions;

/**
 * @author Golui
 * 
 */

public class Tester
{

	private static final long	serialVersionUID	= -1760060034238296831L;

	/**
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException
	{

		// ResultSet rs =
		// DatabaseConnection.safeExecuteQuery(DatabaseConnection.pokedexStatement,
		// "select * from abilities");
		//
		// while (rs.next())
		// {
		// System.out.println(rs.getString("identifier"));
		// }

		TriggerFunctions.init();
		BattlePokemon charizard = new BattlePokemon();

		charizard.nature = EnumNature.TIMID;

		charizard.setStats(Pokemon.BASE, 78, 84, 78, 109, 85, 100);
		charizard.setStats(Pokemon.IV, 31, 15, 31, 31, 31, 31);
		charizard.setStats(Pokemon.EV, 4, 0, 0, 252, 0, 252);

		charizard.types.add(Type.FIRE);
		charizard.types.add(Type.FLYING);

		charizard.setStatChange(Pokemon.SPATK, 0);

		BattlePokemon glaceon = new BattlePokemon();

		glaceon.nature = EnumNature.HARDY;

		glaceon.setStats(Pokemon.BASE, 65, 60, 110, 130, 95, 65);
		glaceon.setStats(Pokemon.IV, 31, 31, 31, 31, 31, 31);

		glaceon.types.add(Type.ICE);

		glaceon.setStatChange(Pokemon.SPDEF, 0);

		charizard.level = 100;
		glaceon.level = 100;

		charizard.recomputeStats();
		glaceon.recomputeStats();

		Move ancientpower = DatabaseReader.readMoveFromDatabase("Solar Beam");

		ancientpower.triggers = new ArrayList<Trigger>();

		ancientpower.triggers.add(TriggerFunctions.functions.get(1));

		Turn t = new Turn();

		t.battle = new SingleBattle();

		t.battle.dealMinDamage = true;

		System.out.println(t.calculateDamage(charizard, glaceon, ancientpower));

	}
}
