package net.golui.pokemonbattle.pokemon;

import static net.golui.pokemonbattle.pokemon.Pokemon.ATK;
import static net.golui.pokemonbattle.pokemon.Pokemon.DEF;
import static net.golui.pokemonbattle.pokemon.Pokemon.SPATK;
import static net.golui.pokemonbattle.pokemon.Pokemon.SPD;
import static net.golui.pokemonbattle.pokemon.Pokemon.SPDEF;

/**
 * @author Golui
 * 
 */

public enum EnumNature
{
	BASHFUL(0, -1, -1),
	LONELY(1, ATK, DEF),
	ADAMANT(2, ATK, SPATK),
	NAUGHTY(3, ATK, SPDEF),
	BRAVE(4, ATK, SPD),
	BOLD(5, DEF, ATK),
	DOCILE(6, -1, -1),
	IMPISH(7, DEF, SPATK),
	LAX(8, DEF, SPDEF),
	RELAXED(9, DEF, SPD),
	MODEST(10, SPATK, ATK),
	MILD(11, SPATK, DEF),
	HARDY(12, -1, -1),
	RASH(13, SPATK, SPDEF),
	QUIET(14, SPATK, SPD),
	CALM(15, SPDEF, ATK),
	GENTLE(16, SPDEF, DEF),
	CAREFUL(17, SPDEF, SPATK),
	QUIRKY(18, -1, -1),
	SASSY(19, SPDEF, SPD),
	TIMID(20, SPD, ATK),
	HASTY(21, SPD, DEF),
	JOLLY(22, SPD, SPATK),
	NAIVE(23, SPD, SPDEF),
	SERIOUS(24, -1, -1);

	byte	id;
	byte	boost;
	byte	nerf;

	EnumNature(int i, int positive, int negative)
	{
		id = (byte) i;
		boost = (byte) positive;
		nerf = (byte) negative;
	}
}
