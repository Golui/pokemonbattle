package net.golui.pokemonbattle.pokemon;

import java.util.ArrayList;
import java.util.List;

import net.golui.pokemonbattle.trigger.Trigger;

/**
 * @author Golui
 * 
 */

public class Move
{

	public static final byte	STATUS					= 1;
	public static final byte	PHYSICAL				= 2;
	public static final byte	SPECIAL					= 3;

	public int					id;
	public String				name;
	public Type					moveType;
	public int					maxPP;

	public short				basePower;
	public short				accuracy;
	public byte					damageType;
	public boolean				isMultiTarget			= false;
	public Object[]				statChange;
	public byte					priority				= 0;
	public byte					secondaryEffectChance	= 0;
	public List<Trigger>		triggers				= new ArrayList<Trigger>();
	// Unused as of 16.04.2014
	public int					targetId;

	/**
	 * @return
	 */
	public List<Trigger> getTriggers()
	{
		return triggers;
	}

	/**
	 * @return
	 */
	public byte getDamageType()
	{
		return damageType;
	}

	/**
	 * @return
	 */
	public Type getType()
	{
		return moveType;
	}

	public short getBasePower()
	{
		return basePower;
	}

	/**
	 * Structure: stat changes will be applied to the <b>user</b>, until there
	 * is a {@code boolean} of value {@code true}. It then applies stat changes
	 * to the target.
	 * 
	 * @return
	 */
	public Object[] getStatChange()
	{
		return statChange;
	}
}
