package net.golui.pokemonbattle.pokemon;

public enum Type
{
	UNKNOWN(-1),
	NORMAL(0),
	FIGHTING(1),
	FLYING(2),
	POISON(3),
	GROUND(4),
	ROCK(5),
	BUG(6),
	GHOST(7),
	STEEL(8),
	FIRE(9),
	WATER(10),
	GRASS(11),
	ELECTRIC(12),
	PSYCHIC(13),
	ICE(14),
	DRAGON(15),
	DARK(16),
	FAIRY(17);

	public final static byte	NRM					= 0;
	public final static byte	DBL					= 1;
	public final static byte	HLF					= 2;
	public final static byte	ZRO					= 3;

	// public final static Modifier[] effectiveness = { 1, 2, 0.5f, 0 };
	public final static int[]	effectiveness		= { 0x1000, 0x2000, 0x800, 0x0 };

	public static final int[][]	effectivenessTable	= {
			{ NRM, NRM, NRM, NRM, NRM, HLF, NRM, ZRO, HLF, NRM, NRM, NRM, NRM, NRM, NRM, NRM, NRM, NRM }, // Normal
			{ DBL, NRM, HLF, HLF, NRM, DBL, HLF, ZRO, DBL, NRM, NRM, NRM, NRM, HLF, DBL, NRM, DBL, HLF }, // Fighting
			{ NRM, DBL, NRM, NRM, NRM, HLF, DBL, NRM, HLF, NRM, NRM, DBL, HLF, NRM, NRM, NRM, NRM, NRM }, // Flying
			{ NRM, NRM, NRM, HLF, HLF, HLF, NRM, HLF, ZRO, NRM, NRM, DBL, NRM, NRM, NRM, NRM, NRM, DBL }, // Poison
			{ NRM, NRM, ZRO, DBL, NRM, DBL, HLF, NRM, HLF, ZRO, NRM, NRM, DBL, NRM, NRM, NRM, NRM, NRM }, // Ground
			{ NRM, HLF, DBL, NRM, HLF, NRM, DBL, NRM, HLF, DBL, NRM, NRM, NRM, NRM, DBL, NRM, NRM, NRM }, // Rock
			{ NRM, HLF, HLF, HLF, NRM, NRM, NRM, HLF, HLF, HLF, NRM, DBL, NRM, DBL, NRM, NRM, DBL, HLF }, // Bug
			{ ZRO, NRM, NRM, NRM, NRM, NRM, NRM, DBL, NRM, NRM, NRM, NRM, NRM, DBL, NRM, NRM, HLF, NRM }, // Ghost
			{ NRM, NRM, NRM, NRM, NRM, DBL, NRM, NRM, HLF, HLF, HLF, NRM, HLF, NRM, DBL, NRM, NRM, DBL }, // Steel
			{ NRM, NRM, NRM, NRM, NRM, HLF, DBL, NRM, DBL, HLF, HLF, DBL, NRM, NRM, DBL, HLF, NRM, NRM }, // Fire
			{ NRM, NRM, NRM, NRM, DBL, DBL, NRM, NRM, NRM, DBL, HLF, HLF, NRM, NRM, NRM, HLF, NRM, NRM }, // Water
			{ NRM, NRM, HLF, HLF, DBL, DBL, HLF, NRM, HLF, HLF, DBL, HLF, NRM, NRM, NRM, HLF, NRM, NRM }, // Grass
			{ NRM, NRM, DBL, NRM, ZRO, NRM, NRM, NRM, NRM, NRM, DBL, HLF, HLF, NRM, NRM, HLF, NRM, NRM }, // Electric
			{ NRM, DBL, NRM, DBL, NRM, NRM, NRM, NRM, HLF, NRM, NRM, NRM, NRM, HLF, NRM, NRM, ZRO, NRM }, // Psychic
			{ NRM, NRM, DBL, NRM, DBL, NRM, NRM, NRM, HLF, HLF, HLF, DBL, NRM, NRM, HLF, DBL, NRM, NRM }, // Ice
			{ NRM, NRM, NRM, NRM, NRM, NRM, NRM, NRM, HLF, NRM, NRM, NRM, NRM, NRM, NRM, DBL, NRM, ZRO }, // Dragon
			{ NRM, HLF, NRM, NRM, NRM, NRM, NRM, DBL, NRM, NRM, NRM, NRM, NRM, DBL, NRM, NRM, HLF, HLF }, // Dark
			{ NRM, DBL, NRM, HLF, NRM, NRM, NRM, NRM, HLF, HLF, NRM, NRM, NRM, NRM, NRM, DBL, DBL, NRM }, // Fairy
													};

	int							id					= -1;

	private Type(int id)
	{
		this.id = id;
	}

	public static int getEffectiveness(Type atk, Type... defending)
	{
		int m = 0x1000;

		for (Type t : defending)
		{
			m = ((m * effectiveness[effectivenessTable[atk.id][t.id]]) + 0x800) >> 12;
		}

		return m;
	}
}
