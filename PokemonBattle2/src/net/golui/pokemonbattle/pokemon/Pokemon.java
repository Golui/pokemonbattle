package net.golui.pokemonbattle.pokemon;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Golui
 * 
 */

public class Pokemon
{

	public static final byte	BASE		= 0;
	public static final byte	EV			= 1;
	public static final byte	IV			= 2;

	public static final byte	HP			= 0;
	public static final byte	ATK			= 1;
	public static final byte	DEF			= 2;
	public static final byte	SPATK		= 3;
	public static final byte	SPDEF		= 4;
	public static final byte	SPD			= 5;

	// public byte baseHP;
	// public byte baseAttack;
	// public byte baseDefense;
	// public byte baseSpecialAttack;
	// public byte baseSpecialDefense;
	// public byte baseSpeed;
	//
	// public byte evHP;
	// public byte evAttack;
	// public byte evDefense;
	// public byte evSpecialAttack;
	// public byte evSpecialDefense;
	// public byte evSpeed;
	//
	// public byte ivHP;
	// public byte ivAttack;
	// public byte ivDefense;
	// public byte ivSpecialAttack;
	// public byte ivSpecialDefense;
	// public byte ivSpeed;
	//
	// public short compHP;
	// public short compAttack;
	// public short compDefense;
	// public short compSpecialAttack;
	// public short compSpecialDefense;
	// public short compSpeed;

	public short[][]			stats		= { { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 } };

	public short[]				computed	= { 0, 0, 0, 0, 0, 0 };

	public EnumNature			nature;

	public int					level;

	public List<Type>			types		= new ArrayList<Type>();

	public List<Move>			moveset		= new ArrayList<Move>();

	public Pokemon setStats(int which, int hp, int atk, int def, int spatk, int spdef, int spd)
	{

		// if (which != BASE || which != EV || which != IV) return null;

		stats[which][HP] = (short) hp;
		stats[which][ATK] = (short) atk;
		stats[which][DEF] = (short) def;
		stats[which][SPATK] = (short) spatk;
		stats[which][SPDEF] = (short) spdef;
		stats[which][SPD] = (short) spd;

		return this;
	}

	public void recomputeStats()
	{
		// Calculate HP. If base Hp is 1, then the pokemon is Shedinja and set
		// it's hp to 1
		computed[HP] = stats[BASE][HP] == 1 ? 1 : (short) (((2 * stats[BASE][HP] + stats[IV][HP] + stats[EV][HP] / 4.0 + 100) * level) / 100.0 + 10);
		for (int i = 1; i < 6; i++)
		{
			computed[i] = (short) (((2 * stats[BASE][i] + stats[IV][i] + stats[EV][i] / 4.0) * level) / 100.0 + 5);
		}

		if (!(this.nature.boost == -1 || this.nature.nerf == -1))
		{
			computed[this.nature.boost] *= 1.1;
			computed[this.nature.nerf] *= 0.9;
		}

	}

	public short getStat(int which)
	{
		return computed[which];
	}

	/**
	 * @return
	 */
	public HeldItem getItem()
	{
		return new HeldItem();
	}

	/**
	 * @return
	 */
	public Ability getAbility()
	{
		return new Ability();
	}

	/**
	 * @return
	 */
	public List<Type> getTypes()
	{
		return this.types;
	}

}
