package net.golui.pokemonbattle.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Golui
 *
 */

public class DatabaseConnection
{

	private static Connection	pokedexConnection	= null;

	public static Statement		pokedexStatement	= null;

	static
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			DatabaseConnection.pokedexConnection = DriverManager.getConnection("jdbc:sqlite:src/resources/veekun-pokedex.sqlite");
			DatabaseConnection.pokedexStatement = pokedexConnection.createStatement();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static ResultSet safeExecuteQuery(Statement s, String sql)
	{
		try
		{
			return s.executeQuery(sql);
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static int safeExecuteUpdate(Statement s, String sql)
	{
		try
		{
			return s.executeUpdate(sql);
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
}
