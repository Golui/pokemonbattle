package net.golui.pokemonbattle.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.golui.pokemonbattle.pokemon.Move;
import net.golui.pokemonbattle.pokemon.Type;

/**
 * @author Golui
 *
 */

public class DatabaseReader
{

	public static final int	ENGLISH	= 9;

	public static Move readMoveFromDatabase(int id)
	{
		try
		{
			Move m = new Move();
			ResultSet baseData = pollPokedex("select * from moves where id == " + id);
			baseData.next();

			m.moveType = Type.values()[baseData.getInt("type_id")];
			m.basePower = baseData.getShort("power");
			m.accuracy = baseData.getShort("accuracy");
			m.maxPP = baseData.getShort("pp");
			m.priority = (byte) baseData.getShort("priority");
			m.damageType = (byte) baseData.getShort("damage_class_id");
			m.targetId = baseData.getInt("target_id");

			m.name = advance(pollPokedex("select * from move_names where move_id == " + id + " and local_language_id == " + ENGLISH)).getString("name");

			return m;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return null;

	}

	public static Move readMoveFromDatabase(String name)
	{
		try
		{
			ResultSet baseData = pollPokedex("select * from move_names where local_language_id == " + ENGLISH + " and name == '" + name + "'");
			baseData.next();
			return readMoveFromDatabase(baseData.getInt("move_id"));
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static ResultSet pollPokedex(String sql)
	{
		return DatabaseConnection.safeExecuteQuery(DatabaseConnection.pokedexStatement, sql);
	}

	public static ResultSet advance(ResultSet rs) throws SQLException
	{
		rs.next();
		return rs;
	}
}
