package net.golui.pokemonbattle.battle;

import static net.golui.pokemonbattle.pokemon.Pokemon.ATK;
import static net.golui.pokemonbattle.pokemon.Pokemon.DEF;
import static net.golui.pokemonbattle.pokemon.Pokemon.SPATK;
import static net.golui.pokemonbattle.pokemon.Pokemon.SPDEF;
import static net.golui.pokemonbattle.util.Util.applyModifier;
import static net.golui.pokemonbattle.util.Util.chainModifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.golui.pokemonbattle.pokemon.Move;
import net.golui.pokemonbattle.trigger.Trigger;
import net.golui.pokemonbattle.trigger.TriggerFunctions;
import net.golui.pokemonbattle.util.MutableInteger;
import net.golui.pokemonbattle.util.Util;

/**
 * @author Golui
 * 
 */

public class Turn
{

	public static final byte[]	DOMAIN_PROBABILITY		= { 16, 8, 2 };

	public int					lastTriggerIndex		= 0;

	public List<Trigger>		triggers				= new ArrayList<Trigger>();

	public List<TurnEvent>		events					= new ArrayList<TurnEvent>();

	public Battle				battle;

	public boolean				isCurrentHitCritical	= false;

	public byte					criticalHitDomain		= 0;

	public boolean				skipAttackStatBoosts	= false;
	public boolean				skipDefenseStatBoosts	= false;

	public boolean				isCurrentAttackPhysical	= true;

	public short				typeEffectiveness;
	public boolean				isWeatherInEffect		= true;

	/**
	 * @param battle2
	 */
	public Turn(Battle battle)
	{
		this.battle = battle;
	}

	/**
	 * 
	 */
	public Turn()
	{

	}

	public void executeTurn()
	{
		Collections.sort(events);

		for (TurnEvent e : events)
		{
			switch (e.type)
			{
				case TurnEvent.USE_MOVE:
				{
					handleUseMove(e);
				}
				case TurnEvent.USE_ITEM:
				{
					handleUseItem(e);
				}
				case TurnEvent.FLEE:
				{
					handleFlee(e);
				}
				case TurnEvent.SWITCH:
				{
					handleSwitch(e);
				}
				case TurnEvent.MEGA_EVOLUTION:
				{
					handleMegaEvolution(e);
				}
				default:
				{
					// Unreachable
				}
			}
		}
	}

	/**
	 * @param e
	 */
	private void handleMegaEvolution(TurnEvent e)
	{
		// TODO Auto-generated method stub

	}

	/**
	 * @param e
	 */
	private void handleSwitch(TurnEvent e)
	{

	}

	/**
	 * @param e
	 */
	private void handleUseItem(TurnEvent e)
	{
		if (this.battle.canUseItems)
		{

		}
		else
		{

		}
	}

	/**
	 * @param e
	 */
	private void handleFlee(TurnEvent e)
	{
		if (this.battle.isEscapeable)
		{
			// TODO End Battle
		}
		else
		{
			// TODO Handle unescapeable
		}

	}

	/**
	 * @param e
	 */
	private void handleUseMove(TurnEvent e)
	{
		Move m = (Move) e.additionalData[0];
		List<Trigger> list = m.getTriggers();
		list.removeIf(t -> t.triggerType < TriggerFunctions.DIV_ORDER && t.triggerType > TriggerFunctions.DIV_STATUS);

		// MutableInteger accuracy = new MutableInteger(0);
		// MutableInteger evasion = new MutableInteger(0);
		//
		// list.forEach(t ->
		// {
		// if (t.triggerType == TriggerFunctions.ACCURACY_EVASION)
		// Util.safeInvoke(t.triggerFunc, null, this, e.pokemon,
		// e.additionalData[1], m, accuracy, evasion);
		// });

		if (m.accuracy == -1 || Battle.RAND.nextInt(100) < (m.accuracy * ((double) e.pokemon.accuracy) / (e.pokemon.evasion)))
		{
			if (m.getDamageType() != Move.STATUS)
			{
				int damage = calculateDamage(e.pokemon, (BattlePokemon) e.additionalData[1], m);
				// TODO apply damage
			}
			else
			{
				list.forEach(t ->
				{
					if (t.triggerType == TriggerFunctions.STAT_BOOST || t.triggerType == TriggerFunctions.HEAL)
						Util.safeInvoke(t.triggerFunc, null, this, e.pokemon, e.additionalData[1], e.additionalData[2]);
					else Util.safeInvoke(t.triggerFunc, null, this, e.pokemon, e.additionalData[1]);
				});
			}
		}
		else
		{
			// TODO Miss
		}

	}

	/**
	 * The damage calculation formula. Based on
	 * https://www.smogon.com/bw/articles/bw_complete_damage_formula
	 * 
	 * @param atk
	 * @param def
	 * @param m
	 * @return
	 */
	public int calculateDamage(BattlePokemon atk, BattlePokemon def, Move m)
	{

		prepareTriggers(atk, def, m);

		// If the move used is for example a fixed-damage move
		if (triggers.get(0).triggerType == TriggerFunctions.SPECIAL_CASE)
			return calculateTriggerEffect(atk, def, m, TriggerFunctions.SPECIAL_CASE);

		// Calculate the "domain" of critical hit, ie the rate that the hit will
		// be critical
		criticalHitDomain = (byte) calculateTriggerEffect(atk, def, m, TriggerFunctions.CRITICAL_HIT_DOMAIN, new MutableInteger(1));

		isCurrentAttackPhysical = m.getDamageType() == Move.PHYSICAL;

		// Determine whether the current hit is critical

		isCurrentHitCritical = criticalHitDomain > 0 && (criticalHitDomain >= 3 || Battle.RAND.nextInt(DOMAIN_PROBABILITY[criticalHitDomain]) == 0);

		// isCurrentHitCritical = false;

		// Prepare the effective base power
		int basePower = applyModifier(calculateTriggerEffect(atk, def, m, TriggerFunctions.BASE_POWER), calculateModifier(atk, def, m, TriggerFunctions.BASE_POWER_MODIFIER));

		// Prepare the Attack or Sp.Attack stat, including Stat Boosts
		int baseAttack = calculateTriggerEffect(atk, def, m, TriggerFunctions.ATTACK_BASE, new MutableInteger(atk.getStat(isCurrentAttackPhysical ? ATK : SPATK)));

		// Prepare the Defense or Sp.Defense stat, including Stat Boosts
		int baseDefense = calculateTriggerEffect(atk, def, m, TriggerFunctions.DEFENSE_BASE, new MutableInteger(def.getStat(isCurrentAttackPhysical ? DEF : SPDEF)));

		// Begin calculating damage; calculate the base damage value
		int damage = ((((2 * atk.level) / 5) + 2) * basePower * baseAttack / baseDefense) / 50 + 2;

		// Begin calculating the modifiers
		int modifier = m.isMultiTarget ? 0xC00 : 0x1000;

		// Calculate the Weather and Chritical Hit modifiers
		modifier = chainModifier(modifier, chainModifier(calculateModifier(atk, def, m, TriggerFunctions.WEATHER), calculateModifier(atk, def, m, TriggerFunctions.CRITICAL_HIT)));

		// Apply the modifier in it's current state and alter with the random
		// factor.
		damage = applyModifier(damage, modifier) * (this.battle.dealMinDamage ? 85 : (100 - Battle.RAND.nextInt(16))) / 100;

		// Calculate the STAB, Type Effectiveness and Burn modifier
		modifier = chainModifier(chainModifier(calculateModifier(atk, def, m, TriggerFunctions.STAB), (typeEffectiveness = (short) calculateModifier(atk, def, m, TriggerFunctions.EFFECTIVENESS))), calculateModifier(atk, def, m, TriggerFunctions.BURN));

		// Apply the modifier
		damage = applyModifier(damage, modifier);

		// Chack if the damage is greater or equal to one
		if (damage < 1) return -1;

		// Apply the final modifier and return the calculated damage
		return applyModifier(damage, calculateModifier(atk, def, m, TriggerFunctions.FINAL));
	}

	/**
	 * @param atk
	 * @param def
	 * @param m
	 */
	private void prepareTriggers(BattlePokemon atk, BattlePokemon def, Move m)
	{
		triggers.clear();

		atk.getItem().getTriggers().forEach(t -> triggers.add(TriggerFunctions.functions.get(t)));
		atk.getAbility().getTriggers().forEach(t -> triggers.add(TriggerFunctions.functions.get(t)));

		def.getItem().getTriggers().forEach(t -> triggers.add(TriggerFunctions.functions.get(t)));
		def.getAbility().getTriggers().forEach(t -> triggers.add(TriggerFunctions.functions.get(t)));

		if (this.battle.weather != null)
			triggers.add(TriggerFunctions.functions.get(this.battle.weather.getTrigger()));

		m.getTriggers().forEach(t -> triggers.add(t));

		triggers.add(TriggerFunctions.functions.get(TriggerFunctions.ATTACK_BOOST_ID));
		triggers.add(TriggerFunctions.functions.get(TriggerFunctions.DEFENSE_BOOST_ID));
		// triggers.add(TriggerFunctions.functions.get(TriggerFunctions.SANDSTORM_ID));
		triggers.add(TriggerFunctions.functions.get(TriggerFunctions.EFFECTIVENESS_ID));
		triggers.add(TriggerFunctions.functions.get(TriggerFunctions.CRITICAL_HIT_ID));
		triggers.add(TriggerFunctions.functions.get(TriggerFunctions.STAB_ID));

		Collections.sort(triggers);
	}

	public int calculateTriggerEffect(BattlePokemon atk, BattlePokemon def, Move m, int triggerType)
	{
		return calculateTriggerEffect(atk, def, m, triggerType, new MutableInteger(0));
	}

	public int calculateModifier(BattlePokemon atk, BattlePokemon def, Move m, int triggerType)
	{
		return calculateTriggerEffect(atk, def, m, triggerType, new MutableInteger(0x1000));
	}

	public int calculateTriggerEffect(BattlePokemon atk, BattlePokemon def, Move m, int triggerType, MutableInteger value)
	{

		int size = triggers.size();

		while (size > lastTriggerIndex)
		{
			Trigger t = triggers.get(lastTriggerIndex);

			if (t.triggerType == triggerType)
			{
				lastTriggerIndex++;
				Util.safeInvoke(t.triggerFunc, null, this, atk, def, m, value);
			}
			else break;
		}

		// System.out.println(triggerType + " " + value.intValue());

		return value.intValue();
	}

	public void filterTriggers()
	{
		this.triggers.removeIf(t -> t.triggerType > TriggerFunctions.DIV_ORDER);
	}
}
