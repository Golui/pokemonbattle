package net.golui.pokemonbattle.battle;

import net.golui.pokemonbattle.pokemon.PartyPokemon;

/**
 * @author Golui
 * 
 */

public class BattlePokemon extends PartyPokemon
{

	public static final short	M6				= 0x400;
	public static final short	M5				= 0x492;
	public static final short	M4				= 0x555;
	public static final short	M3				= 0x666;
	public static final short	M2				= 0x800;
	public static final short	M1				= 0xA98;
	public static final short	N0				= 0x1000;
	public static final short	P1				= 0x1800;
	public static final short	P2				= 0x2000;
	public static final short	P3				= 0x2800;
	public static final short	P4				= 0x3000;
	public static final short	P5				= 0x3800;
	public static final short	P6				= 0x4000;

	public static final short	M6_AE			= 0x555;
	public static final short	M5_AE			= 0x600;
	public static final short	M4_AE			= 0x6DB;
	public static final short	M3_AE			= 0x800;
	public static final short	M2_AE			= 0x999;
	public static final short	M1_AE			= 0xC00;

	public static final short	P1_AE			= 0x1555;
	public static final short	P2_AE			= 0x1AAA;
	public static final short	P3_AE			= 0x2000;
	public static final short	P4_AE			= 0x2555;
	public static final short	P5_AE			= 0x2AAA;
	public static final short	P6_AE			= 0x3000;

	public static final short[]	CHANGE_MODIFIER	= { M6, M5, M4, M3, M2, M1, N0, P1, P2, P3, P4, P5, P6 };

	public int					statChanges		= 0x66666666;

	public static final byte	ACC				= 6;
	public static final byte	EVA				= 7;

	public short				accuracy		= N0;
	public short				evasion			= N0;

	public void setStatChange(int which, int level)
	{
		if (level > 6 || level < -6)
		{
			System.out.println("Trying to set invalid level.");
			return;
		}
		statChanges = (statChanges & ~(0xF * (int) Math.pow(0x10, which))) + (((6 + level) & 0xF) << which * 4);
	}

	public void changeStatChange(int which, int level)
	{
		int current = getStatChange(which);
		setStatChange(which, current + level);

	}

	public short getStatChange(int which)
	{
		return CHANGE_MODIFIER[(statChanges & (0xF * (int) Math.pow(0x10, which))) >> 4 * which];
	}
}
