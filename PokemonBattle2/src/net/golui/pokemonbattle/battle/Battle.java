package net.golui.pokemonbattle.battle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @author Golui
 * 
 */

public abstract class Battle
{

	public List<Trainer>		team1			= new ArrayList<Trainer>();
	public List<Trainer>		team2			= new ArrayList<Trainer>();

	public static final Random	RAND			= new Random();

	public List<Turn>			previusTurns	= new ArrayList<Turn>();

	public BattlePokemon[]		activeTeam1		= new BattlePokemon[3];
	public BattlePokemon[]		activeTeam2		= new BattlePokemon[3];

	public Turn					currentTurn;
	public boolean				isEscapeable;
	public boolean				canUseItems;
	public Weather				weather;
	public boolean				dealMinDamage	= false;

	public Battle()
	{}

	public Battle setTeam(boolean which, Trainer... trainers)
	{
		if (which)
		{
			team1.addAll(Arrays.asList(trainers));
			this.fillTeam(which, activeTeam1, trainers);
		}
		else
		{
			team2.addAll(Arrays.asList(trainers));
		}
		return this;
	}

	/**
	 * @param which
	 * @param activeTeam12
	 * @param trainers
	 */
	public abstract void fillTeam(boolean which, BattlePokemon[] activeTeam12, Trainer[] trainers);

	public void createAndExecuteTurn(TurnEvent... events)
	{
		currentTurn = new Turn(this);
		currentTurn.events.addAll(Arrays.asList(events));
		currentTurn.executeTurn();
	}

}
