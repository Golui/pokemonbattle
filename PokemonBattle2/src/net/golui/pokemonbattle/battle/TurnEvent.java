package net.golui.pokemonbattle.battle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.golui.pokemonbattle.pokemon.Move;
import net.golui.pokemonbattle.pokemon.Pokemon;
import net.golui.pokemonbattle.trigger.Trigger;
import net.golui.pokemonbattle.trigger.TriggerFunctions;
import net.golui.pokemonbattle.util.Util;

/**
 * @author Golui
 * 
 */

public class TurnEvent implements Comparable<TurnEvent>
{

	// Additinal data structure: [Move], [Target(can be self)], [Stat change]
	public static final byte	USE_MOVE		= 0;
	// Additinal data structure: [Pokemon to switch to]
	public static final byte	SWITCH			= 1;
	// Additinal data structure: [Item to use]
	public static final byte	USE_ITEM		= 2;
	// Additinal data structure: None
	public static final byte	MEGA_EVOLUTION	= 3;
	// Additinal data structure: None
	public static final byte	FLEE			= 4;

	public byte					type;
	public byte					priority;
	public short				userSpeed;
	public boolean				firstInBracket	= false;
	public BattlePokemon		pokemon;

	public Object[]				additionalData;

	public List<Trigger>		triggers		= new ArrayList<Trigger>();

	public Turn					turn;

	public TurnEvent()
	{}

	public TurnEvent(Turn turn, int type, BattlePokemon pkm, Object... data)
	{
		this.type = (byte) type;
		additionalData = data.clone();
		priority = 6;
		this.turn = turn;
		if (type == USE_MOVE)
		{
			priority = ((Move) data[0]).priority;
			triggers.addAll(((Move) data[0]).getTriggers());
			triggers.addAll(pkm.getItem().getTriggers());
			triggers.addAll(pkm.getAbility().getTriggers());
		}

		filterTriggers();

		this.userSpeed = (short) (pkm.computed[Pokemon.SPD] * pkm.getStatChange(BattlePokemon.SPD) / 0x1000);

		Collections.sort(triggers);

		triggers.forEach(t ->
		{
			Util.safeInvoke(t.triggerFunc, null, this);
		});

	}

	@Override
	public int compareTo(TurnEvent o)
	{
		int i = this.priority > o.priority ? 1 : this.priority < o.priority ? -1 : 0;
		if (i == 0)
		{
			i = this.firstInBracket ? (o.firstInBracket ? 0 : 1) : (o.firstInBracket ? -1 : 0);
			if (i == 0)
			{
				i = this.userSpeed > o.userSpeed ? 1 : this.userSpeed < o.userSpeed ? -1 : 0;
			}
		}
		return i;
	}

	public void filterTriggers()
	{
		this.triggers.removeIf(t -> t.triggerType != TriggerFunctions.TURN_ORDER);
	}
}
