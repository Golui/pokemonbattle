package net.golui.pokemonbattle.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Golui
 * 
 */

public class Util
{

	public static int chainModifier(int m1, int m2)
	{
		return (m1 * m2 + 0x800) >> 12;
	}

	public static int applyModifier(int value, int modifier)
	{
		return (int) Math.round((value * modifier) / 4096.0);
	}

	/**
	 * Wrapper method to invoke a Method object without try-catch block.
	 * 
	 * @param m
	 *            - Method to invoke
	 * @param invokeOn
	 *            - Object to invoke on (First argument of m.invoke)
	 * @param args
	 *            - Arguments to pass to the called method
	 * @return
	 */
	public static Object safeInvoke(Method m, Object invokeOn, Object... args)
	{
		try
		{
			return m.invoke(invokeOn, args);
		}
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			e.printStackTrace();
			System.out.println();
			throw new RuntimeException("Failed to call the method: " + m.getName());
		}

	}
}
