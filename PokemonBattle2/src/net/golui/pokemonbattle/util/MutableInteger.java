package net.golui.pokemonbattle.util;

/**
 * @author Golui
 * 
 */

public class MutableInteger extends Number implements Comparable<MutableInteger>
{

	private static final long	serialVersionUID	= 7940496733525156601L;
	int							value				= 0;

	public MutableInteger(int i)
	{
		value = i;
	}

	@Override
	public double doubleValue()
	{
		// TODO Auto-generated method stub
		return value;
	}

	@Override
	public float floatValue()
	{
		// TODO Auto-generated method stub
		return value;
	}

	@Override
	public int intValue()
	{
		// TODO Auto-generated method stub
		return value;
	}

	@Override
	public long longValue()
	{
		// TODO Auto-generated method stub
		return value;
	}

	public MutableInteger multiply(int i)
	{
		this.value *= i;
		return this;
	}

	public MutableInteger multiply(MutableInteger i)
	{
		this.value *= i.value;
		return this;
	}

	public MutableInteger add(int i)
	{
		this.value += i;
		return this;
	}

	public MutableInteger add(MutableInteger i)
	{
		this.value += i.value;
		return this;
	}

	public MutableInteger subtract(int i)
	{
		this.value -= i;
		return this;
	}

	public MutableInteger subtract(MutableInteger i)
	{
		this.value -= i.value;
		return this;
	}

	public MutableInteger divide(int i)
	{
		this.value /= i;
		return this;
	}

	public MutableInteger divide(MutableInteger i)
	{
		this.value /= i.value;
		return this;
	}

	public MutableInteger modulus(int i)
	{
		this.value %= i;
		return this;
	}

	public MutableInteger modulus(MutableInteger i)
	{
		this.value %= i.value;
		return this;
	}

	public MutableInteger set(int i)
	{
		this.value = i;
		return this;
	}

	public MutableInteger increment()
	{
		this.value++;
		return this;
	}

	public MutableInteger decrement()
	{
		this.value--;
		return this;
	}

	@Override
	public int compareTo(MutableInteger arg0)
	{
		return this.value > arg0.value ? 1 : (this.value == arg0.value ? 0 : -1);
	}

}
