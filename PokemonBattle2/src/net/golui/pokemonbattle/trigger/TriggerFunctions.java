package net.golui.pokemonbattle.trigger;

import static net.golui.pokemonbattle.pokemon.Pokemon.ATK;
import static net.golui.pokemonbattle.pokemon.Pokemon.DEF;
import static net.golui.pokemonbattle.pokemon.Pokemon.SPATK;
import static net.golui.pokemonbattle.pokemon.Pokemon.SPDEF;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import net.golui.pokemonbattle.battle.BattlePokemon;
import net.golui.pokemonbattle.battle.Turn;
import net.golui.pokemonbattle.battle.TurnEvent;
import net.golui.pokemonbattle.battle.Weather;
import net.golui.pokemonbattle.pokemon.Move;
import net.golui.pokemonbattle.pokemon.Type;
import net.golui.pokemonbattle.util.MutableInteger;
import net.golui.pokemonbattle.util.Util;

/**
 * @author Golui
 * 
 *         Contains all of the trigger functions for battles.
 * 
 */

public class TriggerFunctions
{

	// TODO
	public static final Class<?>[][]	SIGNATURES			= { { Turn.class, BattlePokemon.class, BattlePokemon.class, Move.class, MutableInteger.class }, { TurnEvent.class }, { Turn.class, BattlePokemon.class, BattlePokemon.class, Object[].class } };

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	/**
	 * Different trigger types. Evaluated from lowest to highest value.
	 */

	//
	// Damage affecting triggers
	// Method arguments: Turn battle, BattlePokemon atk, BattlePokemon def, Move
	// m, MutableInteger value
	//
	public static final byte			SPECIAL_CASE		= 0;
	public static final byte			CRITICAL_HIT_DOMAIN	= 1;
	public static final byte			BASE_POWER			= 2;
	public static final byte			BASE_POWER_MODIFIER	= 3;
	public static final byte			ATTACK_BASE			= 4;
	public static final byte			DEFENSE_BASE		= 5;
	public static final byte			WEATHER				= 6;
	public static final byte			CRITICAL_HIT		= 7;
	public static final byte			STAB				= 8;
	public static final byte			EFFECTIVENESS		= 9;
	public static final byte			BURN				= 10;
	public static final byte			FINAL				= 11;

	/**
	 * Dummy trigger type to make it easier to filter trigger lists
	 */
	public static final byte			DIV_ORDER			= 32;

	//
	// Turn order affecting triggers
	// Arguments: TurnEvent
	//
	public static final byte			TURN_ORDER			= 33;
	//
	// Trigger a status effect
	// Arguments: Turn, BattlePokemon user, BattlePokemon target,Move m,
	// MutableInteger value
	//
	public static final byte			STATUS_EFFECT		= 34;
	//
	// Trigger a status effect
	// Arguments: Turn, BattlePokemon user, BattlePokemon target, Object[]
	// statChange
	//
	public static final byte			STAT_BOOST			= 35;
	public static final byte			HEAL				= 36;
	public static final byte			ACCURACY_EVASION	= 37;
	public static final byte			DIV_STATUS			= 64;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	/**
	 * Fixed trigger IDs. IDs from 0 - 32 are reserved. This is to prevent
	 * shifting Trigger IDs (Here and in the Database) in case new internal ones
	 * would need to be created. This is purely for convenience.
	 */
	/***/
	public static final int				ATTACK_BOOST_ID		= 65536;
	public static final int				DEFENSE_BOOST_ID	= 65537;
	public static final int				SANDSTORM_ID		= 65538;
	public static final int				BURN_ID				= 65539;
	public static final int				EFFECTIVENESS_ID	= 65540;
	public static final int				STAB_ID				= 65541;
	public static final int				CRITICAL_HIT_ID		= 65542;

	public static final int				FIXED_BASE_POWER_ID	= 1;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	/**
	 * Shorthand for lowest or highest trigger priority. Applies only withing a
	 * group.
	 */
	/***/
	public static final byte			LO_P				= -128;
	public static final byte			HI_P				= 127;

	/**
	 * Contains the actual Method objects
	 */
	public static Map<Integer, Trigger>	functions;

	/**
	 * Prepares the functions list.
	 */
	public static void init()
	{
		Method[] mth = TriggerFunctions.class.getMethods();

		TriggerFunctions.functions = new HashMap<Integer, Trigger>();
		System.out.println("Adding Methods...");
		for (Method m : mth)
		{
			if (m.isAnnotationPresent(TriggerFunction.class))
			{
				Trigger t = new Trigger(m);
				if (functions.size() > 0 && functions.get(t.triggerID) != null)
				{
					throw new RuntimeException("Dublicate trigger id! Conflict: " + functions.get(t.triggerID).triggerFunc.getName() + " and " + m.getName() + " (id = " + t.triggerID + ")");
				}
				verifySignature(t);
				System.out.println("Added method \"" + m.getName() + '"' + " with id " + t.triggerID);
				functions.put(new Integer(t.triggerID), t);
			}
		}
	}

	public static void verifySignature(Trigger t)
	{
		boolean b = true;
		int signatureIndex = t.triggerType == TURN_ORDER ? 1 : (t.triggerType < DIV_STATUS && t.triggerType > DIV_ORDER ? 2 : 0);
		String s = "";
		Class<?>[] parameterTypes = t.triggerFunc.getParameterTypes();

		for (int i = 0; i < parameterTypes.length; i++)
		{
			s += parameterTypes[i].getName() + ";";
			if (SIGNATURES[signatureIndex][i] != parameterTypes[i])
			{
				b = false;
			}
		}
		if (!b)
		{
			throw new RuntimeException("Invalid method signature at id " + t.triggerID + " Was:" + s.substring(0, s.length() - 1));
		}
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// TRIGGER DEFINITIONS START HERE
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// INTERNAL
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	@TriggerFunction(triggerID = ATTACK_BOOST_ID, priority = HI_P, triggerType = ATTACK_BASE)
	public static void INTERNAL_applyAttackBoost(Turn battle, BattlePokemon atk, BattlePokemon def, Move m, MutableInteger value)
	{
		int change = atk.getStatChange(battle.isCurrentAttackPhysical ? ATK : SPATK);
		if (battle.skipAttackStatBoosts && change > 0x1000) change = 0x1000;
		value.set(Util.applyModifier(value.intValue(), change));
	}

	@TriggerFunction(triggerID = DEFENSE_BOOST_ID, priority = HI_P, triggerType = DEFENSE_BASE)
	public static void INTERNAL_applyDefenseBoost(Turn battle, BattlePokemon atk, BattlePokemon def, Move m, MutableInteger value)
	{
		int change = def.getStatChange(battle.isCurrentAttackPhysical ? DEF : SPDEF);
		if ((battle.skipDefenseStatBoosts || battle.isCurrentHitCritical) && change > 0x1000) change = 0x1000;
		value.set(Util.applyModifier(value.intValue(), change));
	}

	@TriggerFunction(triggerID = SANDSTORM_ID, priority = 64, triggerType = DEFENSE_BASE)
	public static void INTERNAL_applySandstormDefenseBoost(Turn battle, BattlePokemon atk, BattlePokemon def, Move m, MutableInteger value)
	{
		if (battle.isWeatherInEffect && battle.battle.weather.type == Weather.SANDSTORM && def.getTypes().contains(Type.ROCK) && !battle.isCurrentAttackPhysical)
		{
			value.set(Util.applyModifier(value.intValue(), 0x1800));
		}
	}

	@TriggerFunction(triggerID = BURN_ID, priority = HI_P, triggerType = BURN)
	public static void INTERNAL_applyBurnEffect(Turn battle, BattlePokemon atk, BattlePokemon def, Move m, MutableInteger value)
	{
		if (battle.isCurrentAttackPhysical) value.set(0x800);
	}

	@TriggerFunction(triggerID = EFFECTIVENESS_ID, priority = HI_P, triggerType = EFFECTIVENESS)
	public static void INTERNAL_applyTypeEffectiveness(Turn battle, BattlePokemon atk, BattlePokemon def, Move m, MutableInteger value)
	{
		value.set(Util.applyModifier(value.intValue(), Type.getEffectiveness(m.getType(), def.getTypes().toArray(new Type[def.getTypes().size()]))));
	}

	@TriggerFunction(triggerID = STAB_ID, priority = HI_P, triggerType = STAB)
	public static void INTERNAL_applySTAB(Turn battle, BattlePokemon atk, BattlePokemon def, Move m, MutableInteger value)
	{
		value.set(atk.getTypes().contains(m.getType()) ? 0x1800 : 0x1000);
	}

	@TriggerFunction(triggerID = CRITICAL_HIT_ID, priority = HI_P, triggerType = CRITICAL_HIT)
	public static void INTERNAL_criticalHitValue(Turn battle, BattlePokemon atk, BattlePokemon def, Move m, MutableInteger value)
	{
		value.set(battle.isCurrentHitCritical ? 0x1800 : 0x1000);
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// DAMAGE CALCULATION
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	@TriggerFunction(triggerID = FIXED_BASE_POWER_ID, triggerType = BASE_POWER)
	public static void fixedBasePower(Turn battle, BattlePokemon atk, BattlePokemon def, Move m, MutableInteger value)
	{
		value.set(m.getBasePower());
	}
}
