package net.golui.pokemonbattle.trigger;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Golui
 * 
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TriggerFunction
{

	int triggerID();

	byte triggerType();

	/**
	 * @return
	 */
	byte priority() default 0;
}
