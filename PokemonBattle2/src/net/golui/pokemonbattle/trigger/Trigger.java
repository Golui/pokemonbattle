package net.golui.pokemonbattle.trigger;

import java.lang.reflect.Method;

/**
 * @author Golui
 * 
 *         Object representiang a reference to a trigger function.
 * 
 *         Trigger function is a short function applied under certain
 *         circumstances.
 */

public class Trigger implements Comparable<Trigger>
{

	// /**
	// * Internal triggers.
	// */
	// public static final Trigger ATTACK_STAT_BOOST = new Trigger(ATTACK_BASE,
	// TriggerFunctions.ATTACK_BOOST_ID);
	//
	// public static final Trigger DEFENSE_STAT_BOOST = new
	// Trigger(DEFENSE_BASE, TriggerFunctions.DEFENSE_BOOST_ID);
	//
	// public static final Trigger SANDSTORM_MODIFIER = new
	// Trigger(DEFENSE_BASE, TriggerFunctions.SANDSTORM);
	//
	// public static final Trigger BURN_MODIFIER = new Trigger(BURN,
	// TriggerFunctions.BURN);
	//
	// public static final Trigger EFFECTIVENESS_MODIFIER = new
	// Trigger(EFFECTIVENESS, TriggerFunctions.EFFECTIVENESS);
	//
	// public static final Trigger STAB_TRIGGER = new Trigger(STAB,
	// TriggerFunctions.STAB);
	//
	// public static final Trigger CRITICAL_HIT_VALUE = new
	// Trigger(CRITICAL_HIT, TriggerFunctions.CRITICAL_HIT);

	public byte		triggerType;
	public byte		triggerPriority;
	public int		triggerID;
	public Method	triggerFunc;

	/**
	 * @param m
	 */
	public Trigger(Method m)
	{
		TriggerFunction tf = m.getAnnotation(TriggerFunction.class);
		this.triggerID = tf.triggerID();
		this.triggerType = tf.triggerType();
		this.triggerPriority = tf.priority();
		this.triggerFunc = m;
	}

	@Override
	public int compareTo(Trigger arg0)
	{
		return this.triggerType > arg0.triggerType ? 1 : (this.triggerType == arg0.triggerType ? comparePriority(arg0) : -1);
	}

	private int comparePriority(Trigger arg0)
	{
		return this.triggerPriority > arg0.triggerPriority ? 1 : (this.triggerPriority == arg0.triggerPriority ? compareID(arg0) : -1);
	}

	private int compareID(Trigger arg0)
	{
		return this.triggerID > arg0.triggerID ? 1 : (this.triggerID == arg0.triggerID ? 0 : -1);
	}
}
